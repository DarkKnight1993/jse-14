package ru.tsc.goloshchapov.tm.comparator;

import ru.tsc.goloshchapov.tm.api.entity.IHasStartDate;

import java.util.Comparator;

public final class ComparatorByStartDate implements Comparator<IHasStartDate> {

    private static final ComparatorByStartDate INSTANCE = new ComparatorByStartDate();

    private ComparatorByStartDate() {
    }

    public static ComparatorByStartDate getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(IHasStartDate o1, IHasStartDate o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getStartDate().compareTo(o2.getStartDate());
    }

}
