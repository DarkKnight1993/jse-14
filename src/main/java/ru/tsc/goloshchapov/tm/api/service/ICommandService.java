package ru.tsc.goloshchapov.tm.api.service;

import ru.tsc.goloshchapov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
